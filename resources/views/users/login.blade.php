<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <title>Login</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body >
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-offset-4 col-md-4" id="box">
                <h2>Đăng nhập website</h2>
                <hr>
                {!! Form::open(array( 'method' => 'post', 'class' => 'form-horizontal')) !!}
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            {{--{!! Form::email('email', $detail['email'], array('class' => 'form-control', 'requires' => 'requires', 'placeholder' => 'Nhập email của bạn')) !!}--}}
                            {!! Form::email('email') !!}

                        </div>
                    </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            {{--{!! Form::password('password', $detail['password'], array('class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nhập mật khẩu của ')) !!}--}}
                        </div>
                    </div >
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            {!! Form::submit('Đăng nhập', array('class' => 'form-control', 'id' => 'sub')) !!}
                        </div>
                    </div>
               {!! Form::close() !!}
            </div>
        </div>

    </div>
</body>
</html>