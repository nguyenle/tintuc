<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\TheLoai;
Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'UserController@index');
Route::group(['prefix' => 'admin'], function (){
    Route::group(['prefix' => 'theloai'], function (){
       Route::get('danhsach', 'TheLoaiController@getList');
       Route::get('sua', 'TheLoaiController@getEdit');
       Route::get('them', 'TheLoaiController@getAdd');
    });

    Route::group(['prefix' => 'loaitin'], function (){
        Route::get('danhsach', 'TheLoaiController@getlist');
        Route::get('sua', 'TheLoaiController@getEdit');
        Route::get('them', 'TheLoaiController@getAdd');
    });

    Route::group(['prefix' => 'tintuc'], function (){
        Route::get('danhsach', 'TheLoaiController@getlist');
        Route::get('sua', 'TheLoaiController@getEdit');
        Route::get('them', 'TheLoaiController@getAdd');
    });

    Route::group(['prefix' => 'comment'], function (){
        Route::get('danhsach', 'TheLoaiController@getlist');
        Route::get('sua', 'TheLoaiController@getEdit');
        Route::get('them', 'TheLoaiController@getAdd');
    });
});
