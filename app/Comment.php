<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $tabel = 'comment';

    //muon biet comment nay thuoc tin tuc nao
    public function tintuc(){
        return $this->belongsTo('App\TinTuc', 'id_tintuc', 'id');
    }

    //comment nay chi thuoc 1 nguoi dung
    public  function user(){
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
}
