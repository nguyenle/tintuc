<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;

class TheLoaiController extends Controller
{
    public function getList(){
        $theloai = TheLoai::all();
        return view('admin.theloai.list', ['theloai' => $theloai]);
    }

    public function getAdd(){
        return view('admin.theloai.add');
    }

    public function getEdit(){
        return view('admin.theloai.edit');
    }
}
