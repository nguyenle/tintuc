<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiTin extends Model
{
    protected $table = 'loaitin';

    //loai tin nay thuoc the loai nao
    public function theloai(){
        return $this->belongsTo('App\TheLoai', 'id_theloai', 'id');
    }

    //ket noi den bang tin tuc, xem trong loai tin nay co bao nhieu tin tuc
    public function tintuc(){
        return $this->hasMany('App\TinTuc', 'id_loaitin', 'id');
    }
}
