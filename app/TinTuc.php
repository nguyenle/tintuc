<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TinTuc extends Model
{
    //bai bao table
    protected $table = 'tintuc';

    public function loaitin(){
        return $this->belongsTo('App\LoaiTin', 'id_loaitin','id');
    }

    //trong tin tuc co bao nhieu commnet
    public function comment(){
        return $this->hasMany('App\Comment', 'id_tintuc', 'id');
    }

}
